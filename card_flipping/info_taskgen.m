function [session_params,trial_params] = info_taskgen(varargin)
%Generates parameters for use in evidence_info_task.m
%argument order:
%diag_length, cat_labels, cat_colors, num_trials, max_prop
session_params = struct;
trial_params = struct;

%default session params
arg_defaults = {8 {'Red','Blue'} {[1 0 0],[0 0 1]} 100 3};
supplied_args = find(~cellfun(@isempty,varargin));
for j = 1:length(arg_defaults)
    if ismember(j,supplied_args)
        arg_defaults(j) = varargin(j);
    end
end

%session stuff
session_params.diag_length = arg_defaults{1};
session_params.cat_labels = arg_defaults{2};
session_params.cat_colors = arg_defaults{3};
session_params.num_trials = arg_defaults{4};
session_params.max_prop = arg_defaults{5};
session_params.num_cats = length(session_params.cat_labels);

%trial stuff
for current_trial = 1:session_params.num_trials
    cat_proportions = zeros(session_params.num_cats,1);
    for j = 1:session_params.num_cats
        cat_proportions(j) = randi(session_params.max_prop);
    end
    
    min_segment = [];
    for j = 1:length(cat_proportions)
        min_segment = [min_segment ones(1,cat_proportions(j))*j];
    end

    trial_flat = repmat(min_segment,1,ceil(session_params.diag_length^2/length(min_segment)));
    shuffle_trial = randperm(session_params.diag_length^2,session_params.diag_length^2);
    trial_params(current_trial).shuffled_trial = trial_flat(shuffle_trial);
end

param_fname = 'evinftask_sessionparamfile.mat';
fname_counter = 0;
while isfile(param_fname)
    fname_counter = fname_counter + 1;
    if fname_counter > 1
        param_fname = char(param_fname(1:end-5) + string(fname_counter) + '.mat');
    else
        param_fname = char(param_fname(1:end-4) + string(fname_counter) + '.mat');
    end
end

save(string(param_fname),'session_params','trial_params');

end

