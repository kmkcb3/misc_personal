function evidence_info_task(subject_number)

%%%%%%%%%%%% study-wide/basic parameters %%%%%%%%%%%%%%
if subject_number == -1
    dev_mode = 1; %1 = enable, 0 = disable
else
    dev_mode = 0;
end

screen_list = Screen('Screens');
dev_screen = 1;
prod_screen = min(screen_list);
edge_offset = 100;
frame_offset = .05;
item_offset = 20;
windowed_scale = .8;
bg_color = 0.5;
frame_thickness = 5; %pixels
font_size = 8;
text_offset = 10;
use_font = 'Consolas';
trial_dration = Inf;
control_button_vertscale = .8; %proportion of space between outer_frame bottom and edge of screen
control_button_horizscale = .15; %proportion of total screen width
trial_data = struct;
exp_dir = pwd;
data_dir = [exp_dir '/data'];
output_filename = [data_dir '/subject_' char(string(subject_number)) '.mat'];
sess_dir = [exp_dir '/session_lists'];
sess_files = dir(sess_dir);
sess_files = sess_files(~ismember({sess_files.name},{'.','..'}));
%cycle through available session lists, assigning subjects sequentially
subject_session = mod(subject_number,length(sess_files));
if subject_session == 0
    subject_session = length(sess_files);
end

%font control
fontlist = listfonts;
if ismember(use_font,fontlist)
    Screen('Preference', 'DefaultFontName', use_font);
    Screen('Preference', 'DefaultFontSize', font_size);
end

%prepare screen
if dev_mode == 1
    Screen('Preference', 'SkipSyncTests', 2);
    current_res = Screen('Resolution',dev_screen);
    [subject_window, subject_rect] = Screen('OpenWindow',dev_screen,[],[0+edge_offset 0+edge_offset (current_res.width*windowed_scale)+edge_offset (current_res.height*windowed_scale)+edge_offset]);
    offscreen_rect_corners = [0+edge_offset 0+edge_offset (current_res.width*windowed_scale)+edge_offset (current_res.height*windowed_scale)+edge_offset];
    control_buttons = {'Close Window'};
else
    Screen('Preference', 'SkipSyncTests', 0);
    current_res = Screen('Resolution',prod_screen);
    [subject_window, subject_rect] = Screen('OpenWindow',prod_screen,[],[0 0 current_res.width current_res.height]);
    offscreen_rect_corners = [0 0 current_res.width current_res.height];
end

%color control and offscreen window prep
Screen('ColorRange',subject_window,1.0)
[drawing_window, ~] = Screen('OpenOffscreenWindow',-1,[],offscreen_rect_corners);
Screen('FillRect',subject_window,bg_color)
Screen('FillRect',drawing_window,bg_color)
Screen('Flip',subject_window)

%%%%%%%%%%%%%%%% session-specific parameters, slow drawing %%%%%%%%%%%%%%%%

%load session file based on subject number
load([sess_dir '/' sess_files(subject_session).name],'session_params','trial_params');

for j = 1:length(session_params.cat_labels)
    control_buttons{length(control_buttons)+1} = session_params.cat_labels{j};
end

%trial display parameter calculation; left, top, right, bottom
outer_frame = [subject_rect(3)*frame_offset subject_rect(4)*frame_offset subject_rect(3)*(1-frame_offset) subject_rect(4)*(1-frame_offset)];

usable_width = outer_frame(3)-outer_frame(1)-item_offset;
usable_height = outer_frame(4)-outer_frame(2)-item_offset;

item_width = (usable_width/session_params.diag_length)-item_offset;
item_height = (usable_height/session_params.diag_length)-item_offset;

row_upper = outer_frame(2)+item_offset;
row_lower = row_upper + item_height;
column_left = outer_frame(1)+item_offset;
column_right = column_left + item_width;

%for now I'm assuming everything is square, could peek at the trial_params
%to handle different shapes but that would require some reworking
current_rect = [column_left, row_upper, column_right, row_lower];
item_rects = cell(session_params.diag_length,session_params.diag_length);
for j = 1:session_params.diag_length
    for k = 1:session_params.diag_length
        item_rects{j,k} = current_rect;
        current_rect(2) = current_rect(4)+item_offset;
        current_rect(4) = current_rect(2)+item_height;
    end
    current_rect(2) = row_upper;
    current_rect(4) = row_lower;
    current_rect(1) = current_rect(3)+item_offset;
    current_rect(3) = current_rect(1)+item_width;
end

%calculate control button rects from outer_frame
control_button_height = (subject_rect(4)-outer_frame(4))*control_button_vertscale;
control_button_width = subject_rect(3)*control_button_horizscale;
horiz_mid = (subject_rect(3)-subject_rect(1))/2;
control_init_left = horiz_mid-(((length(control_buttons)*control_button_width)+((length(control_buttons)-1)*item_offset))/2);
control_init_top = outer_frame(4)+(((subject_rect(4)-outer_frame(4))*(1-control_button_vertscale))/2);
current_control_rect = [control_init_left control_init_top control_init_left+control_button_width control_init_top+control_button_height];
control_button_rects = cell(length(control_buttons),1);
for j = 1:length(control_buttons)
    control_button_rects{j} = current_control_rect;
    current_control_rect(1) = current_control_rect(3) + item_offset;
    current_control_rect(3) = current_control_rect(1) + control_button_width;
end

%calculate button text positions
control_button_text_locs = cell(length(control_buttons),1);
for j = 1:length(control_buttons)
    control_button_text_locs{j} = [control_button_rects{j}(1)+text_offset control_button_rects{j}(2)+(control_button_height*.75)];
end

%make control button text textures and draw offscreen
for j = 1:length(control_buttons)
    Screen('FrameRect', drawing_window, 0, control_button_rects{j}, frame_thickness);
    DrawFormattedText(drawing_window, control_buttons{j}, 'center', 'center', [], [], [], [], [], [], control_button_rects{j});
end
control_button_fullrect = [control_button_rects{1}(1) control_button_rects{1}(2)-1 control_button_rects{length(control_buttons)}(3)+1 control_button_rects{1}(4)];
control_button_matrix = Screen('GetImage',drawing_window,control_button_fullrect);

%draw button text to offscreen textures for later use
control_button_texture = Screen('MakeTexture',subject_window,control_button_matrix);

%begin main trial loop
for current_trial = 1:session_params.num_trials
    %%%%%%%%%%%% trial-specific parameters, within-loop variable %%%%%%%%%%%%%%
    trial_array = reshape(trial_params(current_trial).shuffled_trial,session_params.diag_length,session_params.diag_length);
    init_array_status = zeros(size(trial_array));
    current_array_status = init_array_status;

    %initialize trial data storage
    trial_data(current_trial).array = trial_array;
    trial_data(current_trial).final_array = init_array_status;
    trial_data(current_trial).click_times = [];
    trial_data(current_trial).click_x = [];
    trial_data(current_trial).click_y = [];
    trial_data(current_trial).element_selections = [];
    trial_data(current_trial).missed_clicks = [];

    %draw a trial
    Screen('FrameRect', subject_window, 0, outer_frame, frame_thickness);
    for j = 1:size(trial_array,1)
        for k = 1:size(trial_array,2)
            Screen('FillRect', subject_window, 0, item_rects{j,k});
        end
    end

    Screen('DrawTexture',subject_window,control_button_texture,[],control_button_fullrect)

    %show trial initial state
    Screen('Flip',subject_window,[],1)
    trial_data(current_trial).start_time = GetSecs;

    button_selected = [];
    %poll for button presses and update the display in response
    while GetSecs <= trial_data(current_trial).start_time + trial_dration
        [~, click_x, click_y, ~] = GetClicks(subject_window);
        trial_data(current_trial).click_x = [trial_data(current_trial).click_x; click_x];
        trial_data(current_trial).click_y = [trial_data(current_trial).click_y; click_y];
        trial_data(current_trial).click_times = [trial_data(current_trial).click_times; GetSecs];
        %first check against control buttons
        for j = 1:length(control_buttons)
            if (control_button_rects{j}(1) < click_x) && (click_x < control_button_rects{j}(3)) && (control_button_rects{j}(2) < click_y) && (click_y < control_button_rects{j}(4))
                button_selected = control_buttons{j};
                trial_data(current_trial).missed_clicks = [trial_data(current_trial).missed_clicks; 0];
            end
        end
        if button_selected
            switch button_selected
                case 'Close Window'
                    trial_data(current_trial).final_array = current_array_status;
                    Screen('CloseAll');
                    save(output_filename,'trial_data');
                    return
                case session_params.cat_labels{1}
                    trial_data(current_trial).cat_selection = 1;
                    trial_data(current_trial).final_array = current_array_status;
                    break
                case session_params.cat_labels{2}
                    trial_data(current_trial).cat_selection = 2;
                    trial_data(current_trial).final_array = current_array_status;
                    break
            end
        end
        %then check against array elements
        for j = 1:size(trial_array,1)
            for k = 1:size(trial_array,2)
                if (item_rects{j,k}(1) < click_x) && (click_x < item_rects{j,k}(3)) && (item_rects{j,k}(2) < click_y) && (click_y < item_rects{j,k}(4))
                    current_array_status(j,k) = trial_array(j,k);
                    trial_data(current_trial).element_selections = [trial_data(current_trial).element_selections; j,k];
                    trial_data(current_trial).missed_clicks = [trial_data(current_trial).missed_clicks; 0];
                    Screen('FillRect', subject_window, session_params.cat_colors{trial_array(j,k)}, item_rects{j,k})
                    Screen('Flip', subject_window, [], 1)
                end
            end
        end

        if length(trial_data(current_trial).missed_clicks) < length(trial_data(current_trial).click_times)
            trial_data(current_trial).missed_clicks = [trial_data(current_trial).missed_clicks; 1];
        end
    end
%end main trial loop
end

save(output_filename,'trial_data');

%close stuff
Screen('CloseAll');
clearvars