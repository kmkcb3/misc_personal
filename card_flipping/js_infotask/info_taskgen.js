function info_taskgen(diag_length, cat_labels, cat_colors, num_trials, max_prop)
//Generates parameters for use in evidence_info_task.m
//argument order:
//diagonal length of the stimulus array, category labels, category colors, trial count, maximum category ratio
var session_params = {};
var trial_params = {};

//default session params
let arg_defaults = [8, ['Red','Blue'], [[1, 0, 0],[0, 0, 1]], 100, 3];
for (this_arg = 0; this_arg < arguments.length; this_arg++) {
            if (arguments[this_arg].length != 0) {
                arg_defaults[this_arg] = arguments[this_arg]
            }
};

//session stuff
session_params.diag_length = arg_defaults[0];
session_params.cat_labels = arg_defaults[1];
session_params.cat_colors = arg_defaults[2];
session_params.num_trials = arg_defaults[3];
session_params.max_prop = arg_defaults[4];
session_params.num_cats = session_params.cat_labels.length;

//trial stuff
for (current_trial = 1; current_trial < session_params.num_trials; current_trial++) {
    let cat_proportions = [];
    for (j = 1; j < session_params.num_cats; j++) {
        cat_proportions.push(Math.floor(Math.random()*session_params.max_prop + 1));
    }
    
    let min_segment = [];
    for (j = 1; j < cat_proportions.length; j++) {
        var this_segment = new Array(j).fill(j);
        min_segment.concat(this_segment);
    }

    trial_flat = repmat(min_segment,1,Math.ceil(session_params.diag_length^2/length(min_segment)));
    shuffle_trial = randperm(session_params.diag_length^2,session_params.diag_length^2);
    trial_params(current_trial).shuffled_trial = trial_flat(shuffle_trial);
}

param_fname = 'evinftask_sessionparamfile.mat';
fname_counter = 0;
while isfile(param_fname)
    fname_counter = fname_counter + 1;
    if fname_counter > 1
        param_fname = char(param_fname(1:end-5) + string(fname_counter) + '.mat');
    else
        param_fname = char(param_fname(1:end-4) + string(fname_counter) + '.mat');
    end
end

save(string(param_fname),'session_params','trial_params');

end

