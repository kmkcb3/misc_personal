let canvas = document.querySelector("canvas");
let context = canvas.getContext("2d");
//Can use clientW/H in chrome, but for correct dimensions in FF need to mix and match. Argh. 
canvas.width = document.documentElement.clientWidth
canvas.height = window.innerHeight
context.fillStyle = "red";
context.fillRect(canvas.width*.9,canvas.height*.9,50,50);
context.fillStyle = "blue";
context.fillRect(canvas.width*.01,canvas.height*.01,50,50);

//Browser area determined, establish study-wide values for the display elements - below is not js yet
const frame_offset = .05;
const item_offset = 20;
const bg_color = 0.5;
const frame_thickness = 5; //pixels
const font_size = 8;
const text_offset = 10;
const use_font = 'Consolas';
const trial_dration = Inf;
const control_button_vertscale = .8; //proportion of space between outer_frame bottom and edge of screen
const control_button_horizscale = .15; //proportion of total screen width

//Session-specific values
var control_buttons
for (let j = 1; j < length(session_params.cat_labels); j++) {
    control_buttons(length(control_buttons)+1) = session_params.cat_labels(j);
}

//trial display parameter calculation; left, top, right, bottom
let outer_frame = [subject_rect(3)*frame_offset, subject_rect(4)*frame_offset, subject_rect(3)*(1-frame_offset), subject_rect(4)*(1-frame_offset)];

let usable_width = outer_frame(3)-outer_frame(1)-item_offset;
let usable_height = outer_frame(4)-outer_frame(2)-item_offset;

let item_width = (usable_width/session_params.diag_length)-item_offset;
let item_height = (usable_height/session_params.diag_length)-item_offset;

let row_upper = outer_frame(2)+item_offset;
let row_lower = row_upper + item_height;
let column_left = outer_frame(1)+item_offset;
let column_right = column_left + item_width;

let current_rect = [column_left, row_upper, column_right, row_lower];
let item_rects = cell(session_params.diag_length,session_params.diag_length);
for (let j = 1; j<session_params.diag_length; j++) {
    for (let k = 1; k<session_params.diag_length; k++) {
        item_rects(j,k) = current_rect;
        current_rect(2) = current_rect(4)+item_offset;
        current_rect(4) = current_rect(2)+item_height;
    }
    current_rect(2) = row_upper;
    current_rect(4) = row_lower;
    current_rect(1) = current_rect(3)+item_offset;
    current_rect(3) = current_rect(1)+item_width;
}

//calculate control button rects from outer_frame
let control_button_height = (subject_rect(4)-outer_frame(4))*control_button_vertscale;
let control_button_width = subject_rect(3)*control_button_horizscale;
let horiz_mid = (subject_rect(3)-subject_rect(1))/2;
let control_init_left = horiz_mid-(((length(control_buttons)*control_button_width)+((length(control_buttons)-1)*item_offset))/2);
let control_init_top = outer_frame(4)+(((subject_rect(4)-outer_frame(4))*(1-control_button_vertscale))/2);
let current_control_rect = [control_init_left, control_init_top, control_init_left+control_button_width, control_init_top+control_button_height];
let control_button_rects = cell(length(control_buttons),1);
for (let j = 1; j<length(control_buttons); j++) {
    control_button_rects(j) = current_control_rect;
    current_control_rect(1) = current_control_rect(3) + item_offset;
    current_control_rect(3) = current_control_rect(1) + control_button_width;
}

//calculate button text positions
let control_button_text_locs = cell(length(control_buttons),1);
for (let j = 1; j<length(control_buttons); j++) {
    control_button_text_locs(j) = [control_button_rects(j)(1)+text_offset, control_button_rects(j)(2)+(control_button_height*.75)];
}

//make control button text textures and draw offscreen
for (let j = 1; j<length(control_buttons); j++) {
    Screen('FrameRect', drawing_window, 0, control_button_rects{j}, frame_thickness);
    DrawFormattedText(drawing_window, control_buttons{j}, 'center', 'center', [], [], [], [], [], [], control_button_rects{j});
}

let control_button_fullrect = [control_button_rects(1)(1), control_button_rects(1)(2)-1, control_button_rects(length(control_buttons))(3)+1, control_button_rects(1)(4)];
let control_button_matrix = Screen('GetImage',drawing_window,control_button_fullrect);

//draw button text to offscreen textures for later use
let control_button_texture = Screen('MakeTexture',subject_window,control_button_matrix);

//begin main trial loop
for (let current_trial = 1; current_trial < session_params.num_trials; current_trial++) {
    // trial-specific parameters, within-loop variable
    let trial_array = reshape(trial_params(current_trial).shuffled_trial,session_params.diag_length,session_params.diag_length);
    let init_array_status = zeros(size(trial_array));
    let current_array_status = init_array_status;

    var trial_data
    //initialize trial data storage
    trial_data(current_trial).array = trial_array;
    trial_data(current_trial).final_array = init_array_status;
    trial_data(current_trial).click_times = [];
    trial_data(current_trial).click_x = [];
    trial_data(current_trial).click_y = [];
    trial_data(current_trial).element_selections = [];
    trial_data(current_trial).missed_clicks = [];

    //draw a trial
    Screen('FrameRect', subject_window, 0, outer_frame, frame_thickness);
    for (let j = 1; j<size(trial_array,1); j++) {
        for (let k = 1; k<size(trial_array,2); k++) {
            Screen('FillRect', subject_window, 0, item_rects(j,k));
        }
    }

    Screen('DrawTexture',subject_window,control_button_texture,[],control_button_fullrect)

    //show trial initial state
    Screen('Flip',subject_window,[],1)
    trial_data(current_trial).start_time = GetSecs;

    let button_selected = [];
    //poll for button presses and update the display in response
    while (GetSecs <= trial_data(current_trial).start_time + trial_dration) {
        [~, click_x, click_y, ~] = GetClicks(subject_window);
        trial_data(current_trial).click_x = [trial_data(current_trial).click_x; click_x];
        trial_data(current_trial).click_y = [trial_data(current_trial).click_y; click_y];
        trial_data(current_trial).click_times = [trial_data(current_trial).click_times; GetSecs];
        //first check against control buttons
        for (j = 1; j<length(control_buttons); j++) {
            if ((control_button_rects(j)(1) < click_x) && (click_x < control_button_rects(j)(3)) && (control_button_rects(j)(2) < click_y) && (click_y < control_button_rects(j)(4))) {
                button_selected = control_buttons(j);
                trial_data(current_trial).missed_clicks = [trial_data(current_trial).missed_clicks; 0];
            }
        }
        if (button_selected.length != 0) {
            switch (button_selected) {
                case 'Close Window':
                    trial_data(current_trial).final_array = current_array_status;
                    Screen('CloseAll');
                    save(output_filename,'trial_data');
                    return
                case session_params.cat_labels(1):
                    trial_data(current_trial).cat_selection = 1;
                    trial_data(current_trial).final_array = current_array_status;
                    break
                case session_params.cat_labels(2):
                    trial_data(current_trial).cat_selection = 2;
                    trial_data(current_trial).final_array = current_array_status;
                    break
            }
        }
        //then check against array elements
        for (let j = 1; j<size(trial_array,1); j++) {
            for (let k = 1; k<size(trial_array,2); k++) {
                if ((item_rects(j,k)(1) < click_x) && (click_x < item_rects(j,k)(3)) && (item_rects(j,k)(2) < click_y) && (click_y < item_rects(j,k)(4))) {
                    current_array_status(j,k) = trial_array(j,k);
                    trial_data(current_trial).element_selections.append(j,k);
                    trial_data(current_trial).missed_clicks.append(0);
                    Screen('FillRect', subject_window, session_params.cat_colors(trial_array(j,k)), item_rects(j,k))
                    Screen('Flip', subject_window, [], 1)
                }
            }
        }

        if (length(trial_data(current_trial).missed_clicks) < length(trial_data(current_trial).click_times)) {
            trial_data(current_trial).missed_clicks.append(1);
        }
    }
//end main trial loop
}

save(output_filename,'trial_data');

//close stuff
Screen('CloseAll');