function cluster_mask
debug_mode = 1; %if true will run PTB in windowed mode

resolution=[1920 1080];
grayval=127;
rng('shuffle');
magic_cleanup = onCleanup(@mj_ptb_cleanup);
[w, wRect]=mj_ptb_init(grayval,resolution,debug_mode);

resolution(1)=wRect(3)-wRect(1);
resolution(2)=wRect(4)-wRect(2);


bar_scale = .25;
bar_count = 3;
bar_offset = 0;
bar_x = resolution(1)/bar_count * bar_scale;
bar_y = resolution(2) * bar_scale;
one_bar_dur = 3;
two_bar_dur = 20;

Screen('FillRect',w,[0 127 0; 0 127 0; 0 127 0], [wRect(1) wRect(1)+bar_x wRect(1)+bar_x*2; wRect(2) wRect(2)+bar_offset wRect(2); wRect(1)+bar_x wRect(1)+bar_x*2 wRect(1)+bar_x*3; wRect(2)+bar_y wRect(2)+bar_offset+bar_y wRect(2)+bar_y]);
Screen('Flip',w)
two_bar_im = Screen('GetImage',w);
two_bar_tex = Screen('MakeTexture',w,two_bar_im);
Screen('FillRect',w,[127 0 127; 127 0 127; 127 0 127],[wRect(1) wRect(1)+bar_x wRect(1)+bar_x*2; wRect(2) wRect(2)+bar_offset wRect(2); wRect(1)+bar_x wRect(1)+bar_x*2 wRect(1)+bar_x*3; wRect(2)+bar_y wRect(2)+bar_offset+bar_y wRect(2)+bar_y]);
Screen('Flip',w)
one_bar_im = Screen('GetImage',w);
one_bar_tex = Screen('MakeTexture',w,one_bar_im);

Screen('FillRect',w);
Screen('Flip', w);

for cycles = 1:10
    for frames = 1:two_bar_dur
        Screen('DrawTexture',w,two_bar_tex);
        Screen('Flip',w);
    end
    for frames = 1:one_bar_dur
        Screen('DrawTexture',w,one_bar_tex);
        Screen('Flip',w);
    end
end

%----------------------
function mj_ptb_cleanup

disp('Doing cleanup...');
Screen('CloseAll');
IOPort('CloseAll');
ShowCursor;
fclose('all');
Priority(0);
ListenChar(0);
if IsWin
    ShowHideWinTaskbarMex(1);
end

%---------------------------------------------
function [w, wRect]=mj_ptb_init(grayval,new_resolution,debug_mode)

%do various initializations, pop up a window with the specified background
%  grayscale color (0-255). Returns window pointer

% check for OpenGL compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Get screenNumber of stimulation display. We choose the display with
% the maximum index, which is usually the right one, e.g., the external
% display on a Laptop:
screens=Screen('Screens');
screenNumber=max(screens);

%try to set resolution
if nargin>1 && ~isempty(new_resolution)
    cur_res=Screen('Resolution',screenNumber);
    if cur_res.width ~= new_resolution(1) || cur_res.height ~= new_resolution(2)
        try
            Screen('Resolution',screenNumber,new_resolution(1),new_resolution(2));
        catch %#ok<CTCH>
            button=questdlg(['Unable to set resolution to ' int2str(new_resolution(1)) ' x ' int2str(new_resolution(2)) '. If eyetracking, fixation calculations will likely be inaccurate. Quit or continue anyway?'],'','Quit','Continue','Quit');
            if isempty(button) || strcmp(button,'Quit')
                error('Unable to set resolution; user quit');
            end
        end
    end
end

% Hide the mouse cursor:
HideCursor(screenNumber);

% Returns as default the mean gray value of screen:
grayval=GrayIndex(screenNumber,grayval/255); 

% Open a double buffered fullscreen window on the stimulation screen
% 'screenNumber' and choose/draw a gray background. 'w' is the handle
% used to direct all drawing commands to that window - the "Name" of
% the window. 'wRect' is a rectangle defining the size of the window.
% See "help PsychRects" for help on such rectangles and useful helper
% functions:
if debug_mode
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval, [50 50 (cur_res.width-50) (cur_res.height-50)],[],[],[],[],[],kPsychGUIWindow);
else
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval);
end

% Set text size (Most Screen functions must be called after
% opening an onscreen window, as they only take window handles 'w' as
% input:
Screen('TextSize', w, 32);

%don't echo keypresses to Matlab window
ListenChar(2);

% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(w);
Priority(priorityLevel);

if IsWin
    ShowHideWinTaskbarMex(0);
end