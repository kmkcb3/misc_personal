#read QA-output files and make some plots over time

import glob, os, untangle, collections
import numpy as np
import pandas as pd
from bokeh import plotting,layouts

base_dir = "F:\BoxSync\projects\qa\output\HERMES\output"
plot_dir = "F:\BoxSync\projects\qa"
xml_list = glob.glob(base_dir+'/*/summaryQA.xml')
param_list = ['description','scandate','scantime','mean','SNR','SFNR','std','percentFluc','drift',
              'driftfit','rdc','minCMassX','minCMassY','minCMassZ','maxCMassX','maxCMassY','maxCMassZ',
              'meanCMassX','meanCMassY','meanCMassZ','dispCMassX','dispCMassY','dispCMassZ','driftCMassX',
              'driftCMassY','driftCMassZ','minFWHMX','minFWHMY','minFWHMZ','maxFWHMX','maxFWHMY','maxFWHMZ',
              'meanFWHMX','meanFWHMY','meanFWHMZ','meanGhost','meanBrightGhost']
param_dict = collections.defaultdict(list)

for xml_file in range(len(xml_list)):
    current_summary = untangle.parse(xml_list[xml_file])
    for param in range(len(param_list)):
        if (param_list[param] == 'description') | (param_list[param] == 'scandate') | (param_list[param] == 'scantime'):
            param_val = [value.cdata for ind,value in enumerate(current_summary.XCEDE.analysis.measurementGroup[0].observation) if value['name'] == param_list[param]]
        else:
            param_val = [float(value.cdata) for ind,value in enumerate(current_summary.XCEDE.analysis.measurementGroup[0].observation) if value['name'] == param_list[param]]
        param_dict[param_list[param]].append(param_val[0])

datetime_list = zip(param_dict['scandate'],param_dict['scantime'])
datetime_joined = [''.join([value[0],'T',value[1]]) for value in datetime_list]
param_dict['datetime'] = [np.datetime64(value) for value in datetime_joined]

data_source = pd.DataFrame(param_dict)
asl_data = data_source.loc[lambda data_source: data_source.description=='ASL_KUMC QA', :].reset_index()
ep2d_data = data_source.loc[lambda data_source: data_source.description=='ep2d_bold_KUMC QA', :].reset_index()
moco_data = data_source.loc[lambda data_source: data_source.description=='MoCoSeries', :].reset_index()

plotting.output_file(os.path.join(plot_dir, 'all_summary_plots' + '.html'))

def plot_series(data):
    cmassX = plotting.figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Center of Mass X - ' + data.description[0])
    cmassX.line(x='datetime',y='minCMassX',source=data,line_dash='dotted',color='firebrick',legend='min')
    cmassX.line(x='datetime',y='maxCMassX',source=data,line_dash='dashed',color='green',legend='max')
    cmassX.line(x='datetime',y='meanCMassX',source=data,line_dash='solid',color='navy',legend='avg')
    cmassX.legend.location = "top_left"
    
    cmassY = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Center of Mass Y - ' + data.description[0])
    cmassY.line(x='datetime',y='minCMassY',source=data,line_dash='dotted',color='firebrick',legend='min')
    cmassY.line(x='datetime',y='maxCMassY',source=data,line_dash='dashed',color='green',legend='max')
    cmassY.line(x='datetime',y='meanCMassY',source=data,line_dash='solid',color='navy',legend='avg')
    cmassY.legend.location = "top_left"
    
    cmassZ = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Center of Mass Z - ' + data.description[0])
    cmassZ.line(x='datetime',y='minCMassZ',source=data,line_dash='dotted',color='firebrick',legend='min')
    cmassZ.line(x='datetime',y='maxCMassZ',source=data,line_dash='dashed',color='green',legend='max')
    cmassZ.line(x='datetime',y='meanCMassZ',source=data,line_dash='solid',color='navy',legend='avg')
    cmassZ.legend.location = "top_left"
    
    cmass_drift = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Center of Mass X/Y/Z drift - ' + data.description[0])
    cmass_drift.line(x='datetime',y='driftCMassX',source=data,line_dash='dotted',color='firebrick',legend='X')
    cmass_drift.line(x='datetime',y='driftCMassY',source=data,line_dash='dashed',color='green',legend='Y')
    cmass_drift.line(x='datetime',y='driftCMassZ',source=data,line_dash='solid',color='navy',legend='Z')
    cmass_drift.legend.location = "top_left"
    
    cmass_disp = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Center of Mass X/Y/Z displacement - ' + data.description[0])
    cmass_disp.line(x='datetime',y='dispCMassX',source=data,line_dash='dotted',color='firebrick',legend='X')
    cmass_disp.line(x='datetime',y='dispCMassY',source=data,line_dash='dashed',color='green',legend='Y')
    cmass_disp.line(x='datetime',y='dispCMassZ',source=data,line_dash='solid',color='navy',legend='Z')
    cmass_disp.legend.location = "top_left"
    
    ghosts = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Ghosts - ' + data.description[0])
    ghosts.line(x='datetime',y='meanGhost',source=data,line_dash='dotted',color='firebrick',legend='avg')
    ghosts.line(x='datetime',y='meanBrightGhost',source=data,line_dash='dashed',color='navy',legend='bright')
    ghosts.legend.location = "top_left"
    
    FWHMX = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Smoothness (FWHM) X - ' + data.description[0])
    FWHMX.line(x='datetime',y='minFWHMX',source=data,line_dash='dotted',color='firebrick',legend='min')
    FWHMX.line(x='datetime',y='maxFWHMX',source=data,line_dash='dashed',color='green',legend='max')
    FWHMX.line(x='datetime',y='meanFWHMX',source=data,line_dash='solid',color='navy',legend='avg')
    FWHMX.legend.location = "top_left"
    
    FWHMY = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Smoothness (FWHM) Y - ' + data.description[0])
    FWHMY.line(x='datetime',y='minFWHMY',source=data,line_dash='dotted',color='firebrick',legend='min')
    FWHMY.line(x='datetime',y='maxFWHMY',source=data,line_dash='dashed',color='green',legend='max')
    FWHMY.line(x='datetime',y='meanFWHMY',source=data,line_dash='solid',color='navy',legend='avg')
    FWHMY.legend.location = "top_left"
    
    FWHMZ = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Smoothness (FWHM) Z - ' + data.description[0])
    FWHMZ.line(x='datetime',y='minFWHMZ',source=data,line_dash='dotted',color='firebrick',legend='min')
    FWHMZ.line(x='datetime',y='maxFWHMZ',source=data,line_dash='dashed',color='green',legend='max')
    FWHMZ.line(x='datetime',y='meanFWHMZ',source=data,line_dash='solid',color='navy',legend='avg')
    FWHMZ.legend.location = "top_left"
    
    mag = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Signal magnitude and SNR/SFNR - ' + data.description[0])
    mag.line(x='datetime',y='mean',source=data,line_dash='dotted',color='firebrick',legend='avg')
    mag.line(x='datetime',y='SNR',source=data,line_dash='dashed',color='green',legend='SNR_')
    mag.line(x='datetime',y='SFNR',source=data,line_dash='solid',color='navy',legend='SFNR_')
    mag.legend.location = "top_left"
    
    flux = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='Percent fluctuation, drift, and drift fit - ' + data.description[0])
    flux.line(x='datetime',y='percentFluc',source=data,line_dash='dotted',color='firebrick',legend='% fluctuation')
    flux.line(x='datetime',y='drift',source=data,line_dash='dashed',color='green',legend='drift_val')
    flux.line(x='datetime',y='driftfit',source=data,line_dash='solid',color='navy',legend='drift fit')
    flux.legend.location = "top_left"
    
    roi = plotting.Figure(plot_width=400, plot_height=400,x_axis_type="datetime",title='RDC and relative SD - ' + data.description[0])
    roi.line(x='datetime',y='rdc',source=data,line_dash='dotted',color='firebrick',legend='rdc_roi')
    roi.line(x='datetime',y='std',source=data,line_dash='dashed',color='navy',legend='sd')
    roi.legend.location = "top_left"
    
    return cmassX,cmassY,cmassZ,cmass_drift,cmass_disp,ghosts,FWHMX,FWHMY,FWHMZ,mag,flux,roi
    
moco_plots = plot_series(moco_data)
asl_plots = plot_series(asl_data)
ep2d_plots = plot_series(ep2d_data)

plotting.save(layouts.gridplot([moco_plots, asl_plots, ep2d_plots]))
    
#ehhh   
#scantype_list = [asl_data, ep2d_data, moco_data]
#scantype_name_list = ['asl_data', 'ep2d_data', 'moco_data'] 
#for index in range(3):
#    plotting.output_file(os.path.join(plot_dir,scantype_name_list[index]+'.html'))
#    plot_series(scantype_list[index])