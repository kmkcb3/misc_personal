# -*- coding: utf-8 -*-
# read a bunch of dicom files from some ungodly mess of nonsense folders and sort them into something usable for bxh QA scripts
"""
Created on Tue Apr  3 14:35:09 2018

@author: kkuntzelman2
"""

import glob
import os
import pydicom
import shutil
import subprocess

base_dir = '/home/qa/qa_scans/'
sorted_dir = '/home/karl/Documents/qa/sorted_dicoms/'
script_dir = '/home/karl/Documents/qa/qascripts/'

full_list = glob.glob(os.path.join(base_dir,'**'), recursive=True)
dicom_list = [ file for file in full_list if os.path.isfile(file) ]
series_list = ['ep2d_bold_KUMC QA', 'ASL_KUMC QA', 'MoCoSeries']

for file in dicom_list:
    current_date, current_series = pydicom.read_file(file).StudyDate, pydicom.read_file(file).SeriesDescription
    if current_series in series_list:
        targ_dir_branch = os.path.join(sorted_dir,current_date,current_series).replace(" ","_")
        os.makedirs(targ_dir_branch,exist_ok=True)
        dcm_name = file.rsplit('\\',maxsplit=1)[1]
        if not os.path.exists(os.path.join(targ_dir_branch,dcm_name)):
            shutil.copy2(file,targ_dir_branch)

#cleanup for processed images            
shutil.rmtree(base_dir)
os.makedirs(base_dir)
os.chmod(base_dir,0o766)
       
#start the bxh wrapper and QA script 
subprocess.call(script_dir+'qa_cron')