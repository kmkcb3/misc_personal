image_dir = 'F:/BoxSync/projects/dodd/visual_search/Stimuli/Block 1';
image_type = '.JPG';
image_list = dir([image_dir '/*' image_type]);
bubble_diameter = 82;
bubble_overlap = .5;
theta = 0:pi/50:2*pi;
x_bubble = (bubble_diameter/2)*cos(theta)+round(bubble_diameter/2);
y_bubble = (bubble_diameter/2)*sin(theta)+round(bubble_diameter/2);
bubble_mat = poly2mask(x_bubble, y_bubble, bubble_diameter, bubble_diameter);
full_bubble = repmat(bubble_mat,1,1,3);

%%have x/y inverted somewhere
for image_num = 1:length(image_list)
    this_image_name = image_list(image_num).name;
    this_split_name = strsplit(this_image_name,'.');
    this_image = imread([image_dir '/' this_image_name]);
    if isfolder([image_dir '/' this_split_name{1}]) ~= 1
        mkdir ([image_dir '/' this_split_name{1}]);
    end
    current_x = 1;
    current_y = 1;
    max_x = ceil((size(this_image,1)-bubble_diameter)/(bubble_diameter*(1-bubble_overlap)));
    max_y = ceil((size(this_image,2)-bubble_diameter)/(bubble_diameter*(1-bubble_overlap)));
    for this_row = 1:max_x
        for this_column = 1:max_y
            image_chunk = this_image(current_x:current_x+bubble_diameter-1,current_y:current_y+bubble_diameter-1,:);
            image_chunk(full_bubble==0)=0;
            imwrite(image_chunk,[image_dir '/' this_split_name{1} '/' this_split_name{1} '_row' num2str(this_row) '_column' num2str(this_column) '.JPEG'],'JPEG');
            current_y = current_y+(bubble_diameter*(1-bubble_overlap));
        end
        current_y = 1;
        current_x = current_x+(bubble_diameter*(1-bubble_overlap));
    end
end