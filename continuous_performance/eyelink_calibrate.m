function eyelink_calibrate
%shamelessly stolen from Evan's c3p0 project

grayval=127;
resolution=[1280 1024]; %x-resolution, y-resolution; must provide both


%==================
% init Psychtoolbox
%==================
magic_cleanup = onCleanup(@mj_ptb_cleanup);
[w, wRect]=mj_ptb_init(grayval,resolution,false); %#ok<ASGLU>

%====================================
% tell them what to press to continue
%====================================
keypress=mj_prompt_key_code(w,[ 'Press any key to enter Eyelink calibration\n\n'...
                                'When done, press Esc to exit calibration mode\n\n\n\n'...
                                '(Press Q to quit now if you ran this by mistake)']);
if strcmp(KbName(keypress),'q') || strcmp(KbName(keypress),'Q')
    error('User quit task');
end

%actual Eyelink stuff, mostly cribbed from EyelinkImageExample
dummymode = 0;
el=EyelinkInitDefaults(w);
if ~EyelinkInit(dummymode)
    Eyelink('Shutdown');
    error('Could not initialize Eyelink tracker. Maybe you forgot to connect the cable or run the connection setup?');
end

EyelinkDoTrackerSetup(el);

WaitSecs(1);
Eyelink('Shutdown');

mj_prompt_key_code(w,[ 'Calibration appears to have completed OK\n\n'...
                       'Press any key to exit']);



%---------------------------------------------
function [w, wRect]=mj_ptb_init(grayval,new_resolution,debug_mode)

%do various initializations, pop up a window with the specified background
%  grayscale color (0-255). Returns window pointer

% check for OpenGL compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Get screenNumber of stimulation display. We choose the display with
% the maximum index, which is usually the right one, e.g., the external
% display on a Laptop:
screens=Screen('Screens');
screenNumber=max(screens);

%try to set resolution
if nargin>1 && ~isempty(new_resolution)
    cur_res=Screen('Resolution',screenNumber);
    if cur_res.width ~= new_resolution(1) || cur_res.height ~= new_resolution(2)
        try
            Screen('Resolution',screenNumber,new_resolution(1),new_resolution(2));
        catch %#ok<CTCH>
            button=questdlg(['Unable to set resolution to ' int2str(new_resolution(1)) ' x ' int2str(new_resolution(2)) '. If eyetracking, fixation calculations will likely be inaccurate. Quit or continue anyway?'],'','Quit','Continue','Quit');
            if isempty(button) || strcmp(button,'Quit')
                error('Unable to set resolution; user quit');
            end
        end
    end
end

% Hide the mouse cursor:
HideCursor(screenNumber);

% Returns as default the mean gray value of screen:
grayval=GrayIndex(screenNumber,grayval/255); 

% Open a double buffered fullscreen window on the stimulation screen
% 'screenNumber' and choose/draw a gray background. 'w' is the handle
% used to direct all drawing commands to that window - the "Name" of
% the window. 'wRect' is a rectangle defining the size of the window.
% See "help PsychRects" for help on such rectangles and useful helper
% functions:
if debug_mode
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval, [50 50 (cur_res.width-50) (cur_res.height-50)],[],[],[],[],[],kPsychGUIWindow);
else
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval);
end

% Set text size (Most Screen functions must be called after
% opening an onscreen window, as they only take window handles 'w' as
% input:
Screen('TextSize', w, 32);

%don't echo keypresses to Matlab window
ListenChar(2);

% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(w);
Priority(priorityLevel);

if IsWin
    ShowHideWinTaskbarMex(0);
end


%----------------------
function mj_ptb_cleanup

disp('Doing cleanup...');
Screen('CloseAll');
IOPort('CloseAll');
ShowCursor;
fclose('all');
Priority(0);
ListenChar(0);
if IsWin
    ShowHideWinTaskbarMex(1);
end


%-------------------------------------------------
function [code, secs]=mj_prompt_key_code(w,message)

mj_draw_text(w,message);
% [secs, code]=KbWait([],2);
key_is_down = false;
while ~key_is_down
    [key_is_down, secs, code] = KbCheck;
    WaitSecs(.001);
end
code=find(code);


%--------------------------------
function mj_draw_text(w, message)

DrawFormattedText(w, message, 'center', 'center', WhiteIndex(w));
Screen('Flip', w);
