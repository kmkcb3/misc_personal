%draft of continuous performance task, kk jan 2019
function cont_perf_task

    %%%%%%%%%%%% study-wide/basic parameters %%%%%%%%%%%%%%
    screen_list = Screen('Screens');
    dev_screen = 0;
    prod_screen = min(screen_list);
    windowed_scale = .8;
    edge_offset = 100;
    bg_color = 0.5;
    circle_color = 0;
    exp_dir = pwd;
    data_dir = [exp_dir '/data'];
    if isdir(data_dir) ~= 1
        mkdir(data_dir);
    end
    
    %define some properties for multi-arc display
    num_arcs = 6;
    num_intervals = 720;
    interval_length = 50;
    break_point = num_intervals/2;
    arc_length = 350;
    directions = [1 -1];
    arc_rects = zeros(num_arcs,4);
    arc_starts = zeros(num_arcs,1);
    arc_lengths = zeros(num_arcs,1);
    rotation_direction = zeros(num_arcs,1);
    rotation_speed = zeros(num_arcs,1);
    arc_spacing = 2;
    arc_step = 2;
    frame_speed = 0.05;
    
    %check existing data files to provide default subject number
    data_files = dir(data_dir);
    used_subject_numbers = [];
    for dfile = 1:length(data_files)
        this_name = strsplit(data_files(dfile).name,{'_', '.'});
        if length(this_name) > 2
            used_subject_numbers = [used_subject_numbers str2double(this_name{2})];
        end
    end
    
    if max(used_subject_numbers) > 0
        subject_number = max(used_subject_numbers)+1;
    else
        subject_number = 1;
    end
    
    %allow experimenter tweaking/confirmation of settings
    param_list = {'Subject number: ', ...
                  'Number of arcs: ', ...
                  'Number of intervals: ', ...
                  'Interval length (frames): ', ...
                  'Length of arcs (degrees): ', ...
                  'Arc spacing: ', ...
                  'Arc step size (per frame): ', ...
                  'Frame speed (seconds per frame): '};
    param_defaults = {num2str(subject_number), num2str(num_arcs), num2str(num_intervals), num2str(interval_length), num2str(arc_length), num2str(arc_spacing), num2str(arc_step), num2str(frame_speed)};
    parameter_settings = inputdlg(param_list, 'Experiment settings', 1, param_defaults);
    if isempty(parameter_settings)
        clearvars
        disp('Run canceled.');
        return
    end
    subject_number = str2num(parameter_settings{1});
    num_arcs = str2num(parameter_settings{2});
    num_intervals = str2num(parameter_settings{3});
    interval_length = str2num(parameter_settings{4});
    arc_length = str2num(parameter_settings{5});
    arc_spacing = str2num(parameter_settings{6});
    arc_step = str2num(parameter_settings{7});
    frame_speed = str2num(parameter_settings{8});
    while subject_number==used_subject_numbers
        new_subj_number = inputdlg('Please enter an unused subject number: ','Data file already exists!',1,num2str(subject_number));
        subject_number = str2num(new_subj_number);
    end
    if isempty(subject_number)
        clearvars
        disp('Run canceled.');
        return
    end
    output_filename = [data_dir '/subject_' char(string(subject_number)) '.mat'];

    % Eyelink file naming
    edf_name_pattern         = 'circ%.2d%.edf'; % DOS, so 8.3 characters limit (8 name, 3 extension)

    %prepare screen
    if subject_number == -1
        dev_mode = 1; %1 = enable, 0 = disable
        do_eye_tracking = 0;
    else
        dev_mode = 0;
        do_eye_tracking = 1;
        eyelink_calibrate
    end
    
    if dev_mode == 1
        Screen('Preference', 'SkipSyncTests', 2);
        current_res = Screen('Resolution',dev_screen);
        [subject_window, subject_rect] = Screen('OpenWindow',dev_screen,[],[0+edge_offset 0+edge_offset (current_res.width*windowed_scale)+edge_offset (current_res.height*windowed_scale)+edge_offset]);
        offscreen_rect_corners = [0+edge_offset 0+edge_offset (current_res.width*windowed_scale)+edge_offset (current_res.height*windowed_scale)+edge_offset];
    else
        Screen('Preference', 'SkipSyncTests', 0);
        current_res = Screen('Resolution',prod_screen);
        [subject_window, subject_rect] = Screen('OpenWindow',prod_screen,[],[0 0 current_res.width current_res.height]);
        offscreen_rect_corners = [0 0 current_res.width current_res.height];
    end
    
    %%finish defining arcs with screen info
    full_x = subject_rect(3)-subject_rect(1);
    full_y = subject_rect(4)-subject_rect(2);
    for current_arc = 1:num_arcs
        if current_arc == 1
            arc_rects(current_arc,:) = subject_rect;
        else
            arc_rects(current_arc,:) = [subject_rect(1)+full_x/(current_arc*arc_spacing) subject_rect(2)+full_y/(current_arc*arc_spacing) subject_rect(3)-full_x/(current_arc*arc_spacing) subject_rect(4)-full_y/(current_arc*arc_spacing)];
        end
        arc_starts(current_arc) = current_arc*10-10;
        arc_lengths(current_arc) = arc_length;
        rotation_direction(current_arc) = directions(randi(length(directions)));
        rotation_speed(current_arc) = rand(1)*randi(2);
    end
    current_positions = arc_starts;

    %color control and offscreen window prep
    Screen('ColorRange',subject_window,1.0)
    [drawing_window, ~] = Screen('OpenOffscreenWindow',-1,[],offscreen_rect_corners);
    Screen('FillRect',subject_window,bg_color)
    Screen('FillRect',drawing_window,bg_color)
    Screen('Flip',subject_window)

    

    %%prepare for response collection
    KbName('UnifyKeyNames');
    %key_list = zeros(1,256);
    %key_list(KbName('space')) = 1;
    %KbQueueCreate([],key_list);
    KbQueueCreate();

    %initialize eyetracker
    if do_eye_tracking
        dummymode = 0;
        eyelink_info=EyelinkInitDefaults(subject_window);
        if ~EyelinkInit(dummymode)
            Eyelink('Shutdown');
            error('Eyelink Init aborted.');
        else
            disp('Eyelink successfully initialized.');
        end
        edf_output_name = sprintf(edf_name_pattern, subject_number);
        Eyelink('Openfile', edf_output_name);
    end

    %=============
    %ready to start the task, prompt to proceed
    %=============
    keypress=mj_prompt_key_code(subject_window,[ 'Ready to begin task\n\n'...
                                'If you need to make any changes, press Q to quit now\n\n\n\n'...
                                'Press any other key to begin']);
    if strcmp(KbName(keypress),'q') || strcmp(KbName(keypress),'Q')
        error('User quit task');
    end

    % start eye tracker
    if do_eye_tracking
        Eyelink('StartRecording');
    end

    %draw arcs
    for k = 1:num_arcs
        Screen('FrameArc',subject_window,circle_color,arc_rects(k,:),arc_starts(k),arc_lengths(k), 5);
    end
    disp_start = Screen('Flip',subject_window);
    start_time = GetSecs();
    KbQueueStart();

    %rotate arcs on each frame and save spacebar responses
    frame_time = zeros(num_intervals,interval_length);
    press_time = zeros(num_intervals,interval_length);
    arc_location = zeros(num_intervals,interval_length,num_arcs);
    for interval = 1:num_intervals
        %segment eyetracking data by interval
        if do_eye_tracking
            Eyelink('Message', sprintf('TRIALID %.3d',interval) );
            Eyelink('Message', sprintf('SYNCTIME') );
        end

        for j = 1:interval_length
            for k = 1:num_arcs
                current_positions(k) = current_positions(k)+arc_step*rotation_speed(k)*rotation_direction(k);
                Screen('FrameArc',subject_window,circle_color,arc_rects(k,:),current_positions(k),arc_lengths(k), 5);
                arc_location(interval,j,k) = current_positions(k);
            end
            disp_start = disp_start+frame_speed;
            [~,this_press_time,~,~,~] = KbQueueCheck();
            press_time(interval,j) = this_press_time(KbName('space'));
            frame_time(interval,j) = Screen('Flip',subject_window,disp_start)-start_time;
        end
        flip_arc = randi(num_arcs);
        rotation_direction(flip_arc) = rotation_direction(flip_arc)*-1;
        for k = 1:num_arcs
            rotation_speed(k) = rand(1)*randi(2);
        end
        if do_eye_tracking
            Eyelink('Message', sprintf('TRIAL_END %.3d',interval) );
        end
        if interval == break_point
            break_duration = take_break(subject_window);
            disp_start = disp_start + break_duration;
        end
    end
    KbQueueStop();
    if dev_mode == 0
        save(output_filename);
    end

    %shut down the eyetracker
    if do_eye_tracking
        Eyelink('StopRecording');
        Eyelink('CloseFile');
        Eyelink('ReceiveFile',edf_output_name,pwd,1);
        fprintf('File received: %s\n\n', fullfile(data_dir,edf_output_name) );
        Eyelink('Shutdown');
        disp('Eyelink has been shut down');
    end

    Screen('CloseAll')

function break_duration = take_break(w)
break_start_time = GetSecs();
mj_draw_text(w,['Please keep your head still, but take a minute now to rest your eyes if you need it.\n\n'...
                'When you are ready to continue, press any key and the task will resume.']);
WaitSecs(1);
key_is_down = false;
while ~key_is_down
    [key_is_down, break_end_time, ~] = KbCheck;
    WaitSecs(.001);
break_duration = break_end_time - break_start_time - .001;
end


function eyelink_calibrate
%shamelessly stolen from Evan's c3p0 project

grayval=127;
resolution=[1280 1024]; %x-resolution, y-resolution; must provide both


%==================
% init Psychtoolbox
%==================
magic_cleanup = onCleanup(@mj_ptb_cleanup);
[w, wRect]=mj_ptb_init(grayval,resolution,false); %#ok<ASGLU>

%====================================
% tell them what to press to continue
%====================================
keypress=mj_prompt_key_code(w,[ 'Press any key to enter Eyelink calibration\n\n'...
                                'When done, press Esc to exit calibration mode\n\n\n\n'...
                                '(Press Q to quit now if you ran this by mistake)']);
if strcmp(KbName(keypress),'q') || strcmp(KbName(keypress),'Q')
    error('User quit task');
end

%actual Eyelink stuff, mostly cribbed from EyelinkImageExample
dummymode = 0;
el=EyelinkInitDefaults(w);
if ~EyelinkInit(dummymode)
    Eyelink('Shutdown');
    error('Could not initialize Eyelink tracker. Maybe you forgot to connect the cable or run the connection setup?');
end

EyelinkDoTrackerSetup(el);

WaitSecs(1);
Eyelink('Shutdown');

mj_prompt_key_code(w,[ 'Calibration appears to have completed OK\n\n'...
                       'Press any key to exit']);



%---------------------------------------------
function [w, wRect]=mj_ptb_init(grayval,new_resolution,debug_mode)

%do various initializations, pop up a window with the specified background
%  grayscale color (0-255). Returns window pointer

% check for OpenGL compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Get screenNumber of stimulation display. We choose the display with
% the maximum index, which is usually the right one, e.g., the external
% display on a Laptop:
screens=Screen('Screens');
screenNumber=max(screens);

%try to set resolution
if nargin>1 && ~isempty(new_resolution)
    cur_res=Screen('Resolution',screenNumber);
    if cur_res.width ~= new_resolution(1) || cur_res.height ~= new_resolution(2)
        try
            Screen('Resolution',screenNumber,new_resolution(1),new_resolution(2));
        catch %#ok<CTCH>
            button=questdlg(['Unable to set resolution to ' int2str(new_resolution(1)) ' x ' int2str(new_resolution(2)) '. If eyetracking, fixation calculations will likely be inaccurate. Quit or continue anyway?'],'','Quit','Continue','Quit');
            if isempty(button) || strcmp(button,'Quit')
                error('Unable to set resolution; user quit');
            end
        end
    end
end

% Hide the mouse cursor:
HideCursor(screenNumber);

% Returns as default the mean gray value of screen:
grayval=GrayIndex(screenNumber,grayval/255); 

% Open a double buffered fullscreen window on the stimulation screen
% 'screenNumber' and choose/draw a gray background. 'w' is the handle
% used to direct all drawing commands to that window - the "Name" of
% the window. 'wRect' is a rectangle defining the size of the window.
% See "help PsychRects" for help on such rectangles and useful helper
% functions:
if debug_mode
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval, [50 50 (cur_res.width-50) (cur_res.height-50)],[],[],[],[],[],kPsychGUIWindow);
else
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval);
end

% Set text size (Most Screen functions must be called after
% opening an onscreen window, as they only take window handles 'w' as
% input:
Screen('TextSize', w, 32);

%don't echo keypresses to Matlab window
ListenChar(2);

% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(w);
Priority(priorityLevel);

if IsWin
    ShowHideWinTaskbarMex(0);
end


%----------------------
function mj_ptb_cleanup

disp('Doing cleanup...');
Screen('CloseAll');
IOPort('CloseAll');
ShowCursor;
fclose('all');
Priority(0);
ListenChar(0);
if IsWin
    ShowHideWinTaskbarMex(1);
end


%-------------------------------------------------
function [code, secs]=mj_prompt_key_code(w,message)

mj_draw_text(w,message);
% [secs, code]=KbWait([],2);
key_is_down = false;
while ~key_is_down
    [key_is_down, secs, code] = KbCheck;
    WaitSecs(.001);
end
code=find(code);


%--------------------------------
function mj_draw_text(w, message)

DrawFormattedText(w, message, 'center', 'center', WhiteIndex(w));
Screen('Flip', w);
