%draft of continuous performance task, kk jan 2019
function cont_perf_training
    %%%%%%%%%%%% study-wide/basic parameters %%%%%%%%%%%%%%
    dev_mode = 0;
    do_eye_tracking = 0;
    

    screen_list = Screen('Screens');
    dev_screen = 0;
    prod_screen = min(screen_list);
    windowed_scale = .8;
    edge_offset = 100;
    bg_color = 0.5;
    circle_color = 0;

    %prepare screen
    if dev_mode == 1
        Screen('Preference', 'SkipSyncTests', 2);
        current_res = Screen('Resolution',dev_screen);
        [subject_window, subject_rect] = Screen('OpenWindow',dev_screen,[],[0+edge_offset 0+edge_offset (current_res.width*windowed_scale)+edge_offset (current_res.height*windowed_scale)+edge_offset]);
        offscreen_rect_corners = [0+edge_offset 0+edge_offset (current_res.width*windowed_scale)+edge_offset (current_res.height*windowed_scale)+edge_offset];
    else
        Screen('Preference', 'SkipSyncTests', 0);
        current_res = Screen('Resolution',prod_screen);
        [subject_window, subject_rect] = Screen('OpenWindow',prod_screen,[],[0 0 current_res.width current_res.height]);
        offscreen_rect_corners = [0 0 current_res.width current_res.height];
    end

    %color control and offscreen window prep
    Screen('ColorRange',subject_window,1.0)
    [drawing_window, ~] = Screen('OpenOffscreenWindow',-1,[],offscreen_rect_corners);
    Screen('FillRect',subject_window,bg_color)
    Screen('FillRect',drawing_window,bg_color)
    Screen('Flip',subject_window)

    %define some properties for multi-arc display
    num_arcs = 6;
    num_intervals = 10;
    interval_length = 50;
    arc_length = 350;
    directions = [1 -1];
    full_x = subject_rect(3)-subject_rect(1);
    full_y = subject_rect(4)-subject_rect(2);
    arc_rects = zeros(num_arcs,4);
    arc_starts = zeros(num_arcs,1);
    arc_lengths = zeros(num_arcs,1);
    rotation_direction = zeros(num_arcs,1);
    rotation_speed = zeros(num_arcs,1);
    arc_spacing = 2;
    for current_arc = 1:num_arcs
        if current_arc == 1
            arc_rects(current_arc,:) = subject_rect;
        else
            arc_rects(current_arc,:) = [subject_rect(1)+full_x/(current_arc*arc_spacing) subject_rect(2)+full_y/(current_arc*arc_spacing) subject_rect(3)-full_x/(current_arc*arc_spacing) subject_rect(4)-full_y/(current_arc*arc_spacing)];
        end
        arc_starts(current_arc) = current_arc*10-10;
        arc_lengths(current_arc) = arc_length;
        rotation_direction(current_arc) = directions(randi(length(directions)));
        rotation_speed(current_arc) = rand(1)*randi(2);
    end
    current_positions = arc_starts;
    arc_step = 2;

    %%prepare for response collection
    KbName('UnifyKeyNames');
    %key_list = zeros(1,256);
    %key_list(KbName('space')) = 1;
    %KbQueueCreate([],key_list);
    KbQueueCreate();

    %=============
    %ready to start the task, prompt to proceed
    %=============
    keypress=mj_prompt_key_code(subject_window,[ 'Ready to begin training\n\n'...
                                'Your goal is to press the space bar whenever the gaps in two adjacent rings line up\n\n\n\n'...
                                'Press any key to begin']);
    
    % start eye tracker
    if do_eye_tracking
        Eyelink('StartRecording');
    end

    %draw arcs
    for k = 1:num_arcs
        Screen('FrameArc',subject_window,circle_color,arc_rects(k,:),arc_starts(k),arc_lengths(k), 5);
    end
    disp_start = Screen('Flip',subject_window);
    start_time = GetSecs();
    KbQueueStart();

    %rotate arcs on each frame and save spacebar responses
    frame_time = zeros(num_intervals,interval_length);
    press_time = zeros(num_intervals,interval_length);
    arc_location = zeros(num_intervals,interval_length,num_arcs);
    for interval = 1:num_intervals

        for j = 1:interval_length
            for k = 1:num_arcs
                current_positions(k) = current_positions(k)+arc_step*rotation_speed(k)*rotation_direction(k);
                Screen('FrameArc',subject_window,circle_color,arc_rects(k,:),current_positions(k),arc_lengths(k), 5);
                arc_location(interval,j,k) = current_positions(k);
            end
            disp_start = disp_start+.05;
            [~,this_press_time,~,~,~] = KbQueueCheck();
            press_time(interval,j) = this_press_time(KbName('space'));
            frame_time(interval,j) = Screen('Flip',subject_window,disp_start)-start_time;
        end
        flip_arc = randi(num_arcs);
        rotation_direction(flip_arc) = rotation_direction(flip_arc)*-1;
        for k = 1:num_arcs
            rotation_speed(k) = rand(1)*randi(2);
        end
    end
    KbQueueStop();

    Screen('CloseAll')


%---------------------------------------------
function [w, wRect]=mj_ptb_init(grayval,new_resolution,debug_mode)

%do various initializations, pop up a window with the specified background
%  grayscale color (0-255). Returns window pointer

% check for OpenGL compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Get screenNumber of stimulation display. We choose the display with
% the maximum index, which is usually the right one, e.g., the external
% display on a Laptop:
screens=Screen('Screens');
screenNumber=max(screens);

%try to set resolution
if nargin>1 && ~isempty(new_resolution)
    cur_res=Screen('Resolution',screenNumber);
    if cur_res.width ~= new_resolution(1) || cur_res.height ~= new_resolution(2)
        try
            Screen('Resolution',screenNumber,new_resolution(1),new_resolution(2));
        catch %#ok<CTCH>
            button=questdlg(['Unable to set resolution to ' int2str(new_resolution(1)) ' x ' int2str(new_resolution(2)) '. If eyetracking, fixation calculations will likely be inaccurate. Quit or continue anyway?'],'','Quit','Continue','Quit');
            if isempty(button) || strcmp(button,'Quit')
                error('Unable to set resolution; user quit');
            end
        end
    end
end

% Hide the mouse cursor:
HideCursor(screenNumber);

% Returns as default the mean gray value of screen:
grayval=GrayIndex(screenNumber,grayval/255); 

% Open a double buffered fullscreen window on the stimulation screen
% 'screenNumber' and choose/draw a gray background. 'w' is the handle
% used to direct all drawing commands to that window - the "Name" of
% the window. 'wRect' is a rectangle defining the size of the window.
% See "help PsychRects" for help on such rectangles and useful helper
% functions:
if debug_mode
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval, [50 50 (cur_res.width-50) (cur_res.height-50)],[],[],[],[],[],kPsychGUIWindow);
else
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval);
end

% Set text size (Most Screen functions must be called after
% opening an onscreen window, as they only take window handles 'w' as
% input:
Screen('TextSize', w, 32);

%don't echo keypresses to Matlab window
ListenChar(2);

% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(w);
Priority(priorityLevel);

if IsWin
    ShowHideWinTaskbarMex(0);
end


%----------------------
function mj_ptb_cleanup

disp('Doing cleanup...');
Screen('CloseAll');
IOPort('CloseAll');
ShowCursor;
fclose('all');
Priority(0);
ListenChar(0);
if IsWin
    ShowHideWinTaskbarMex(1);
end


%-------------------------------------------------
function [code, secs]=mj_prompt_key_code(w,message)

mj_draw_text(w,message);
% [secs, code]=KbWait([],2);
key_is_down = false;
while ~key_is_down
    [key_is_down, secs, code] = KbCheck;
    WaitSecs(.001);
end
code=find(code);


%--------------------------------
function mj_draw_text(w, message)

DrawFormattedText(w, message, 'center', 'center', WhiteIndex(w));
Screen('Flip', w);