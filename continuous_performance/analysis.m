%%analysis
data_dir = 'F:/BoxSync/projects/dodd/continuous performance/continuous_performance/data';
fixations = 'F:/BoxSync/projects/dodd/continuous performance/continuous_performance/fixation_report.txt';
saccades = 'F:/BoxSync/projects/dodd/continuous performance/continuous_performance/saccade_report.txt';
trials = 'F:/BoxSync/projects/dodd/continuous performance/continuous_performance/trial_report.txt';
all_fixation_data = readtable(fixations,'ReadVariableNames',true,'Delimiter','\t');
all_saccade_data = readtable(saccades,'ReadVariableNames',true,'Delimiter','\t');
all_trial_data = readtable(trials,'ReadVariableNames',true,'Delimiter','\t');
subj_num = '1002';
behavior = [data_dir '/subject_' subj_num '.mat'];

load(behavior)
subj_fixations = all_fixation_data(strcmp(all_fixation_data.RECORDING_SESSION_LABEL,['circ' subj_num]),{'RECORDING_SESSION_LABEL','TRIAL_INDEX','CURRENT_FIX_DURATION','CURRENT_FIX_START'});
subj_trials = all_trial_data(strcmp(all_trial_data.RECORDING_SESSION_LABEL,['circ' subj_num]),{'RECORDING_SESSION_LABEL','TRIAL_LABEL','AVERAGE_FIXATION_DURATION', 'AVERAGE_SACCADE_AMPLITUDE', 'BLINK_COUNT', 'DURATION', 'END_TIME', 'FIXATION_COUNT'});

%first convert multi-circle positons to single-circle positions for
%corresponsdence to retinal images and easier math
norm_arc_location = arc_location;
while find(norm_arc_location(:)>360)
    norm_arc_location(norm_arc_location>360)=norm_arc_location(norm_arc_location>360)-360;
end
while find(norm_arc_location(:)<0)
    norm_arc_location(norm_arc_location<0)=norm_arc_location(norm_arc_location<0)+360;
end

%now find partial overlaps pairwise by frame
frames_61 = zeros(num_intervals,interval_length);
frames_23 = zeros(num_intervals,interval_length);
frames_34 = zeros(num_intervals,interval_length);
frames_45 = zeros(num_intervals,interval_length);
frames_56 = zeros(num_intervals,interval_length);

%61 rather than 12 because frame 6 is the outermost; inner starts from 2->
for interval = 1:num_intervals
    frames_61(interval,abs((norm_arc_location(interval,:,6)-norm_arc_location(interval,:,1)))<(.5*(360-arc_length))) = 1;
    frames_23(interval,abs((norm_arc_location(interval,:,2)-norm_arc_location(interval,:,3)))<(.5*(360-arc_length))) = 1;
    frames_34(interval,abs((norm_arc_location(interval,:,3)-norm_arc_location(interval,:,4)))<(.5*(360-arc_length))) = 1;
    frames_45(interval,abs((norm_arc_location(interval,:,4)-norm_arc_location(interval,:,5)))<(.5*(360-arc_length)))= 1;
    frames_56(interval,abs((norm_arc_location(interval,:,5)-norm_arc_location(interval,:,6)))<(.5*(360-arc_length))) = 1;
end

%metrics: total error = summed distance from each alignment to nearest
%press?
%hit % = presence of alignment during each press; miss/FA/correct reject
%as same logic
%for correlation to eye movements - sliding window average?
%alternately, go alignment-by-alignment and look at duration of overlapping
%fixations

%overall
all_alignments = reshape((frames_61+frames_23+frames_34+frames_45+frames_56)',1,[]);
all_response_times = press_time(find(press_time))-start_time;
all_timeline_responses = zeros(size(all_alignments));
all_timeline_responses(find(press_time>0)) = 1;
binary_alignments = all_alignments;
binary_alignments(binary_alignments>1) = 1;
hit_timeline = zeros(size(all_alignments));
hit_timeline(find(binary_alignments==1 & all_timeline_responses==1)) = 1;
cr_timeline = zeros(size(all_alignments));
cr_timeline(find(binary_alignments==0 & all_timeline_responses==0)) = 1;
miss_timeline = zeros(size(all_alignments));
miss_timeline(find(binary_alignments==1 & all_timeline_responses==0)) = 1;
fa_timeline = zeros(size(all_alignments));
fa_timeline(find(binary_alignments==0 & all_timeline_responses==1)) = 1;
all_alignment_times = frame_time(find(all_alignments));
all_time_errors = zeros(size(all_alignment_times));
for this_time = 1:length(all_alignment_times)
    all_time_errors(this_time) = min(abs(all_response_times-all_alignment_times(this_time)));
end
%plot the solid-block SDT - something is obviously wrong with how I am
%calculating these
plot(hit_timeline,'b')
hold on
plot(miss_timeline,'m')
plot(cr_timeline,'k')
plot(fa_timeline,'g')
xlim([0 36000])

%%sliding window
all_timeline_errors = zeros(size(all_alignments));
alignment = 1;
for this_time = 1:length(all_alignments)
    if all_alignments(this_time)
        all_timeline_errors(this_time) = min(abs(all_response_times-all_alignment_times(alignment)));
        alignment = alignment+1;
    end
end

%1 second for now, may need to be longer to make sense for fixations
window_size = 60;
mean_timeline_errors = movmean(all_timeline_errors,window_size);

%%%convert fixation start times to match frame times and create binary
%%%fixation timeline - TODO update to ignore mid-break (trial 361)
%%%fixations, wrap to next trial
subj_fixations.frame_rel_start_time = zeros(height(subj_fixations));
fixation_time = zeros(size(frame_time));
for this_fix = 1:height(subj_fixations)
    %skip fixations during the break period
    if subj_fixations.TRIAL_INDEX(this_fix) == 361 && subj_fixations.CURRENT_FIX_START(this_fix) > 2500
        continue
    end
    
    subj_fixations.frame_rel_start_time(this_fix) = subj_fixations.CURRENT_FIX_START(this_fix)+frame_time(subj_fixations.TRIAL_INDEX(this_fix),1);
    subj_fixations.frame_start(this_fix) = floor(subj_fixations.CURRENT_FIX_START(this_fix)/(frame_speed*1000));
    if subj_fixations.frame_start(this_fix) == 0
        subj_fixations.frame_start(this_fix) = 1;
    end
    subj_fixations.frame_end(this_fix) = subj_fixations.frame_start(this_fix) + ceil(subj_fixations.CURRENT_FIX_DURATION(this_fix)/(frame_speed*1000));
    %check for fixations that overlap epochs, shift remainder to the next
    if subj_fixations.frame_end(this_fix) >50
        epoch_frame_overrun = subj_fixations.frame_end(this_fix)-subj_fixations.frame_start(this_fix)-50;
        fixation_time(subj_fixations.TRIAL_INDEX(this_fix+1),1:epoch_frame_overrun) = 1;
        subj_fixations.frame_end(this_fix) = 50;
    end
    fixation_time(subj_fixations.TRIAL_INDEX(this_fix),subj_fixations.frame_start(this_fix):subj_fixations.frame_end(this_fix)) = 1;
end

fixation_timeline = reshape(fixation_time',1,[]);

%%%by interval
% metrics = struct();
% for this_interval = 1:num_intervals
%     metrics(this_interval).alignments = frames_61(this_interval,:)+frames_23(this_interval,:)+frames_34(this_interval,:)+frames_45(this_interval,:)+frames_56(this_interval,:);
%     metrics(this_interval).response_times = press_time(this_interval,find(press_time(this_interval,:)))-start_time;
%     metrics(this_interval).alignment_times = frame_time(this_interval,find(alignments(this_interval,:)));
%     metrics(this_interval).time_errors = zeros(size(metrics(this_interval).alignment_times));
%     if metrics(this_interval).response_times
%         for this_time = 1:length(metrics(this_interval).alignment_times)
%             metrics(this_interval).time_errors(this_time) = min(abs(metrics(this_interval).response_times-metrics(this_interval).alignment_times(this_time)));
%         end
%     end
%     metrics(this_interval).summed_error = sum(metrics(this_interval).time_errors);
%     metrics(this_interval).mean_error = mean(metrics(this_interval).time_errors);
% end

%%SDT stuff
%hits = zeros(size(press_time));
%misses = zeros(size(press_time));
%false_alarms = zeros(size(press_time));
%correct_rejections = zeros(size(press_time));


